package com.williamle.limelightliteui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application
{
    public static final String VERSION = "1.0";

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        primaryStage.setTitle("Limelight Lite Controller - By William Le");
        Parent root = null;
        try {
            URL location = getClass().getClassLoader().getResource("main.fxml");
            if(location == null)
            {
                alert("Could not load FXML file!",
                "Either the code is pointing to the wrong file, or the resources are not embedded properly.");
                System.exit(1);
            }
            root = FXMLLoader.load(location);
        } catch (Exception e) {
            alert("Failed to init a JavaFX controller!",
            "Check console for log messages. Seems like a software bug.");
            System.exit(1);
        }
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    private void alert(String title, String message)
    {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setTitle("Mukyu!");
        errorAlert.setHeaderText(title);
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }
}
