package com.williamle.limelightliteui;

import com.williamle.limelightliteui.MagicValues.LightMode;
import com.williamle.limelightliteui.MagicValues.Status;
import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import uk.co.caprica.vlcj.player.base.State;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static com.williamle.limelightliteui.MagicValues.DEFAULT_NT_SERVERS;

public class MainController
{
    private static MainController instance;

    @FXML
    private Button toggleautoalign;
    @FXML
    private Rectangle targetdetected;
    @FXML
    private Rectangle autoalignactive;
    @FXML
    private Label coordinates;
    @FXML
    private ChoiceBox<LightMode> modeselect;
    @FXML
    private ColorPicker colorpick;
    @FXML
    private TextArea logbox;
    @FXML
    private Canvas camerastream;
    @FXML
    private ComboBox<String> lllip;
    @FXML
    private ComboBox<String> ntip;
    @FXML
    private Label data;
    @FXML
    private Button resetvideostream;
    @FXML
    private Button resetsystem;

    private ByteArrayOutputStream baos;
    private PrintStream ps;
    private PrintStream console;

    private JavaFXRenderer jfxr;

    private String[] LLL_IPS = new String[] {"10.39.52.85", "192.168.4.2"};

    public MainController()
    {
        // Redirect most of console to GUI
        baos = new ByteArrayOutputStream();
        ps = new PrintStream(baos);
        console = System.out;
        System.setOut(ps);
        System.setErr(ps);

        welcomeMessage();
        NetworkTableHandler.getInstance();
    }

    private void welcomeMessage()
    {
        Log.w("Started LimeLightLiteUI " + Main.VERSION);
        Log.w("Created by William Le (DoggySazHi) for TroyFRC 3952");
    }

    public static MainController getInstance()
    {
        if(instance == null)
            instance = new MainController();
        return instance;
    }

    public void initialize()
    {
        instance = this;

        data.setText("No connection!");
        coordinates.setText("No connection!");

        toggleautoalign.setOnAction(e -> NetworkTableHandler.getInstance().toggleAutoAlign());

        ObservableList<LightMode> options =
                FXCollections.observableArrayList(
                        LightMode.values()
                );
        modeselect.setItems(options);
        modeselect.setValue(LightMode.Idle);

        ObservableList<String> options2 =
                FXCollections.observableArrayList(
                        DEFAULT_NT_SERVERS
                );
        ntip.setItems(options2);
        ntip.setValue(options2.get(0));

        ObservableList<String> options3 =
                FXCollections.observableArrayList(
                        LLL_IPS
                );
        lllip.setItems(options3);
        lllip.setValue(options3.get(0));

        resetStream();

        // Force scroll to bottom
        logbox.textProperty().addListener((ChangeListener<Object>) (observable, oldValue, newValue) -> logbox.setScrollTop(Double.MIN_VALUE));
        modeselect.valueProperty().addListener((ov, old, now) -> NetworkTableHandler.getInstance().setMode(now));
        ntip.valueProperty().addListener((ov, old, now) -> NetworkTableHandler.getInstance().setNTServer(now));
        lllip.valueProperty().addListener((ov, old, now) -> resetStream());
        colorpick.valueProperty().addListener((ov, old, now) -> NetworkTableHandler.getInstance().setColor(now));
        resetvideostream.setOnAction((e) -> resetStream());

        new AnimationTimer() {
            @Override
            public void handle(long currentNanoTime) {
                loop();
            }
        }.start();
    }

    private void resetStream()
    {
        if (jfxr != null)
            jfxr.stop();
        String ip = lllip.getValue();
        jfxr = new JavaFXRenderer(camerastream);
        jfxr.setURL("http://" + ip + ":1181/stream.mjpg");
        jfxr.start();
    }

    public void loop()
    {
        if(!baos.toString().equals(""))
        {
            System.out.flush();
            System.err.flush();
            Log.w(baos.toString());
            System.setOut(console);
            System.out.println(baos);
            System.setOut(ps);
            try {
                baos.flush();
                baos.reset();
                ps.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(jfxr != null && (jfxr.getState() == State.ERROR || jfxr.getState() == State.ENDED))
        {
            Log.w("ERROR", "Failed to get camera stream! Retrying.");
            resetStream();
        }
        modeselect.setValue(NetworkTableHandler.getInstance().getMode());
    }

    public void appendLogText(String st)
    {
        logbox.appendText(st);
    }

    public void setAutoAlignStatus(Status status)
    {
        if (status == Status.DISARMED || status == Status.LOST)
            targetdetected.setFill(new Color(1, 0, 0, 1));
        else
            targetdetected.setFill(new Color(0, 1, 0, 1));
        if (status == Status.DISARMED || status == Status.ARMED)
            autoalignactive.setFill(new Color(1, 0, 0, 1));
        else if (status == Status.ACTIVE)
            autoalignactive.setFill(new Color(0, 1, 0, 1));
        else
            autoalignactive.setFill(new Color(1, 1, 0, 1));
    }

    public void setColorPicker(double r, double g, double b)
    {
        colorpick.setValue(new Color(
                Math.min(1, Math.max(0, r/255.0)),
                Math.min(1, Math.max(0, g/255.0)),
                Math.min(1, Math.max(0, b/255.0)),
                1.0));
    }

    public void setData(String data)
    {
        this.data.setText(data);
    }

    public void setCoordinates(String data)
    {
        coordinates.setText(data);
    }

    public void setNTIP(String ip)
    {
        ntip.setValue(ip);
    }

    public void setLLLIP(String ip)
    {
        lllip.setValue(ip);
    }
}
