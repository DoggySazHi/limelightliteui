package com.williamle.limelightliteui;

import java.nio.ByteBuffer;
import java.util.concurrent.Semaphore;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;

import uk.co.caprica.vlcj.factory.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.base.MediaPlayer;
import uk.co.caprica.vlcj.player.base.State;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.CallbackVideoSurface;
import uk.co.caprica.vlcj.player.embedded.videosurface.VideoSurfaceAdapters;
import uk.co.caprica.vlcj.player.embedded.videosurface.callback.BufferFormat;
import uk.co.caprica.vlcj.player.embedded.videosurface.callback.BufferFormatCallback;
import uk.co.caprica.vlcj.player.embedded.videosurface.callback.RenderCallback;
import uk.co.caprica.vlcj.player.embedded.videosurface.callback.format.RV32BufferFormat;

public class JavaFXRenderer
{
    //Pretty much copied from the vlcj tutorials.

    private String videoURL = "http://10.39.52.85:1081/stream.mjpg";
    private final Canvas canvas;
    private PixelWriter pixelWriter;
    private final WritablePixelFormat<ByteBuffer> pixelFormat;
    private final MediaPlayerFactory mediaPlayerFactory;
    private final EmbeddedMediaPlayer mediaPlayer;
    private WritableImage img;
    private final AnimationTimer timer;

    public JavaFXRenderer(Canvas canvas) {
        this.canvas = canvas;

        pixelWriter = canvas.getGraphicsContext2D().getPixelWriter();
        pixelFormat = PixelFormat.getByteBgraInstance();

        mediaPlayerFactory = new MediaPlayerFactory();
        mediaPlayer = mediaPlayerFactory.mediaPlayers().newEmbeddedMediaPlayer();

        mediaPlayer.videoSurface().set(new JavaFxVideoSurface());

        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                renderFrame();
            }
        };
    }

    public final void setURL(String url)
    {
        videoURL = url;
    }

    public final void start() {
        Log.w("Renderer has been initialized to " + videoURL + "!");
        //mediaPlayer.controls().setRepeat(true);
        String[] options = {
                ":file-caching=0",
                ":network-caching=0",
                ":sout = #transcode{vcodec=x264,vb=800,scale=0.25,acodec=none,fps=24}:display :no-sout-rtp-sap :no-sout-standard-sap :ttl=1 :sout-keep"};
        mediaPlayer.media().play(videoURL, options);

        startTimer();
    }

    public final State getState()
    {
        return mediaPlayer.status().state();
    }

    public final void stop() {
        stopTimer();

        mediaPlayer.controls().stop();
        mediaPlayer.release();
        mediaPlayerFactory.release();
    }

    private class JavaFxVideoSurface extends CallbackVideoSurface {

        JavaFxVideoSurface() {
            super(new JavaFxBufferFormatCallback(), new JavaFxRenderCallback(), true, VideoSurfaceAdapters.getVideoSurfaceAdapter());
        }

    }

    private class JavaFxBufferFormatCallback implements BufferFormatCallback {
        public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
            JavaFXRenderer.this.img = new WritableImage(sourceWidth, sourceHeight);
            JavaFXRenderer.this.pixelWriter = img.getPixelWriter();
            return new RV32BufferFormat(sourceWidth, sourceHeight);
        }

        public void allocatedBuffers(ByteBuffer[] byteBuffers)
        {

        }
    }

    // Semaphore used to prevent the pixel writer from being updated in one thread while it is being rendered by a
    // different thread
    private final Semaphore semaphore = new Semaphore(1);

    // This is correct as far as it goes, but we need to use one of the timers to get smooth rendering (the timer is
    // handled by the demo sub-classes)
    private class JavaFxRenderCallback implements RenderCallback {
        public void display(MediaPlayer mediaPlayer, ByteBuffer[] nativeBuffers, BufferFormat bufferFormat) {
            try {
                semaphore.acquire();
                pixelWriter.setPixels(0, 0, bufferFormat.getWidth(), bufferFormat.getHeight(), pixelFormat, nativeBuffers[0], bufferFormat.getPitches()[0]);
                semaphore.release();
            }
            catch (InterruptedException ignore) {
            }
        }
    }

    protected final void renderFrame() {
        GraphicsContext g = canvas.getGraphicsContext2D();

        double width = canvas.getWidth();
        double height = canvas.getHeight();

        g.setFill(new Color(0, 0, 0, 1));
        g.fillRect(0, 0, width, height);

        if (img != null) {
            double imageWidth = img.getWidth();
            double imageHeight = img.getHeight();

            double sx = width / imageWidth;
            double sy = height / imageHeight;

            double sf = Math.min(sx, sy);

            double scaledW = imageWidth * sf;
            double scaledH = imageHeight * sf;

            Affine ax = g.getTransform();

            g.translate(
                    (width - scaledW) / 2,
                    (height - scaledH) / 2
            );

            if (sf != 1.0) {
                g.scale(sf, sf);
            }

            try {
                semaphore.acquire();
                g.drawImage(img, 0, 0);
                semaphore.release();
            }
            catch (InterruptedException ignore) {
            }

            g.setTransform(ax);

            AutoAlignRenderer.getInstance().draw(g);
        }
    }

    protected void startTimer()
    {
        timer.start();
    }

    protected void stopTimer()
    {
        timer.stop();
    }
}
