package com.williamle.limelightliteui;

public class MagicValues
{
    /*
    So, what do you do with a bunch of final values scattered all over the place?
    Place them in one class, so you Ctrl + F (or Command + F) to go around.

    Everything should be public, static, and final.
     */

    // Disarmed: Missing target, auto-align off.
    // Armed: Found target, auto-align off.
    // Active: Found target, auto-align on.
    // Lost: Missing target, auto-align on. (uh oh)
    public enum Status {DISARMED, ARMED, ACTIVE, LOST}
    public enum LightMode {Idle, AutoAlign, Brownout, Warning, Manual}

    public static final String[] DEFAULT_NT_SERVERS = new String[] {"10.39.52.2", "172.22.11.2", "roboRIO-3952-FRC.lan", "roboRIO-3952-FRC.local", "roboRIO-3952-FRC.frc-field.local"};
    public static final String[] DEBUG_NT_SERVERS = new String[] {"127.0.0.1", "192.168.1.23", "192.168.4.16"};
    public static final double[] DEFAULT_VALUE = new double[] {0.0, 0.0};
    public static final double[] DEFAULT_CENTER_COORDINATES = new double[] {320.0, 240.0};

    public static final int ULTRASONIC_SAMPLES = 20;
    //NOTE: IN CM
    public static final double DROP_OUTLIER_THRESHOLD = 500;
    public static final int FAILS_TO_FLUSH = 10;
}
