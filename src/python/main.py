# To see messages from networktables, you must setup logging
import logging
import sys
import threading
import time
from signal import *

import RPi.GPIO as GPIO
import board
import neopixel
from lightController import *
from networktables import NetworkTables

# I hate Python. - William Le

# --- CONSTANTS
ULTRASONIC_TRIGGER = 23
ULTRASONIC_ECHO = 24
LIGHT_PIN = board.D18
NUM_LEDS = 24
NT_IPS = ["10.39.52.2",
          "172.22.11.2",
          "roboRIO-3952-FRC.lan",
          "roboRIO-3952-FRC.local",
          "roboRIO-3952-FRC.frc-field.local",
          "127.0.0.1",
          "192.168.1.23",
          "192.168.4.16"]
# In seconds.
ULTRASONIC_TIMEOUT = 5
# ---

GPIO.setmode(GPIO.BCM)
GPIO.setup(ULTRASONIC_TRIGGER, GPIO.OUT)
GPIO.setup(ULTRASONIC_ECHO, GPIO.IN)

pixels = neopixel.NeoPixel(LIGHT_PIN, NUM_LEDS)

logging.basicConfig(level=logging.DEBUG)

r = 0
g = 0
b = 0
mode = 'Idle'
stop = False


def valueChanged(table, key, value, isNew):
    global r, g, b, mode
    # print("valueChanged: key: '%s'; value: %s; isNew: %s" % (key, value, isNew))
    if key == 'Red':
        r = max(0, min(value, 255))
    if key == 'Green':
        g = max(0, min(value, 255))
    if key == 'Blue':
        b = max(0, min(value, 255))
    if key == 'Mode':
        mode = value


def connectionListener(connected, info):
    print(info, "; Connected=%s" % connected)


def distance():
    # --- Emit the "ping!"
    GPIO.output(ULTRASONIC_TRIGGER, True)
    # --- Keep the emitter on so something's actually emitted
    time.sleep(0.00001)
    # --- Stop emitting the "ping!"
    GPIO.output(ULTRASONIC_TRIGGER, False)
    # --- Keep updating until the "ping!" leaves.
    timeout = time.time()
    start = time.time()
    while GPIO.input(ULTRASONIC_ECHO) == 0:
        start = time.time()
        if time.time() - timeout > ULTRASONIC_TIMEOUT:
            return 0
    # --- Keep updating until the "ping!" comes back.
    # --- (at this point it's a "pong!")
    end = time.time()
    while GPIO.input(ULTRASONIC_ECHO) == 1:
        end = time.time()
        if time.time() - timeout > ULTRASONIC_TIMEOUT:
            return 0
    # --- Do some math calculations
    # --- (in a nutshell, take time diff, multiply by speed of US wave,
    # --- divide by double distance since it went out and in)
    return ((end - start) * 34300) / 2


NetworkTables.initialize(NT_IPS)

NetworkTables.addConnectionListener(connectionListener, immediateNotify=True)
sd = NetworkTables.getTable("cameralight")
sd2 = NetworkTables.getTable("LimeLightLite")
sd.addEntryListener(valueChanged)
sd2.addEntryListener(valueChanged)

thread = threading.Thread(target=idleAnim,args=(pixels,),name='idleAnim')
thread.start()

# got really bored


def cameraLightMain():
    # python where are my switches >:[
    global mode, stop, pixels, thread, r, g, b
    while True:
        if mode == 'Idle':
            if thread.name != 'idleAnim':
                print('Switched to idle animation')
                kill()
                thread.join()
                thread = threading.Thread(target=idleAnim, args=(pixels,), name='idleAnim')
                thread.start()
        elif mode == 'AutoAlign':
            if thread.name != 'autoAlignAnim':
                print('Switched to AA animation')
                kill()
                thread.join()
                thread = threading.Thread(target=autoAlignAnim, args=(pixels,),name='autoAlignAnim')
                thread.start()
        elif mode == 'Brownout':
            if thread.name != 'brownOutAnim':
                print('Switched to brownout animation')
                kill()
                thread.join()
                thread = threading.Thread(target=brownOutAnim, args=(pixels,), name='brownOutAnim')
                thread.start()
        elif mode == 'Warning':
            if thread.name != 'warningAnim':
                print('Switched to warning animation')
                kill()
                thread.join()
                thread = threading.Thread(target=warningAnim, args=(pixels,), name='warningAnim')
                thread.start()
        else:
            if thread.name != 'manualAnim':
                print('Switched to manual animation')
                kill()
                thread.join()
                thread = threading.Thread(target=manualAnim, args=(pixels,), name='manualAnim')
                thread.start()
            setRGB(r, g, b)
        if stop:
            kill()
            print("Attempting to merge")
            thread.join()
            print("Light thread killed")
            return


def ultrasonicMain():
    while True:
        if stop:
            print("US thread killed")
            break
        sd.putNumber("Ultrasonic Distance", distance())
        # For the safety of the sensor readings, we sleep
        time.sleep(0.5)


t1 = threading.Thread(target=cameraLightMain, name='cameraLightMain')
t2 = threading.Thread(target=ultrasonicMain, name='ultrasonicMain')

t1.start()
t2.start()


def shutdown(*args):
    global stop
    print("Stopping, please wait.")
    stop = True
    t1.join()
    t2.join()
    print("Successfully stopped threads.")
    lightsOff(pixels)
    sys.exit(0)


for sig in (SIGABRT, SIGILL, SIGINT, SIGSEGV, SIGTERM):
    signal(sig, shutdown)

while not stop:
    # Make the program not quit on us, lol.
    try:
        time.sleep(10)
    except KeyboardInterrupt:
        print("Stopping, please wait.")
        stop = True
        t1.join()
        t2.join()
        print("Successfully stopped threads.")
        break

lightsOff(pixels)
