package com.williamle.limelightliteui;

import static com.williamle.limelightliteui.MagicValues.*;

public class UltrasonicSensor
{
    // Handles the numbers coming from the LLL and "sanitizes" them.
    private static UltrasonicSensor instance;
    private double[] rawValues;
    private int currentIndex;
    private int filledValues;
    private int failed;

    private UltrasonicSensor()
    {
        flush();
    }

    public static UltrasonicSensor getInstance()
    {
        if(instance == null)
            instance = new UltrasonicSensor();
        return instance;
    }

    public void updateSensor(double input)
    {
        // If it passes threshold, ignore input. However, let the array be filled first.
        if(filledValues != rawValues.length && Math.abs(getValue() - input) >= DROP_OUTLIER_THRESHOLD) {
            failed++;
            if(failed >= FAILS_TO_FLUSH)
                flush();
            return;
        }
        failed = 0;
        // Set value
        rawValues[currentIndex] = input;
        // Loop index
        currentIndex++;
        if(currentIndex >= rawValues.length)
            currentIndex = 0;
        // Set filled values (denominator of mean function)
        filledValues = Math.min(filledValues + 1, rawValues.length);
    }

    public double getValue()
    {
        if(filledValues == 0)
            return 0;
        double numerator = 0;
        for(double d : rawValues)
            numerator += d;
        return numerator / filledValues;
    }

    public void flush()
    {
        failed = 0;
        rawValues = new double[ULTRASONIC_SAMPLES];
        currentIndex = 0;
        filledValues = 0;
    }
}
