package com.williamle.limelightliteui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class AutoAlignRenderer
{
    private static AutoAlignRenderer instance;
    private static final int SQUARE_SIDE = 50;
    private Point targetPoint;
    private Point centerPoint;
    private Point vector;
    private boolean targetFound;

    private AutoAlignRenderer()
    {
        targetPoint = new Point();
        centerPoint = new Point();
        vector = new Point();
    }

    public static AutoAlignRenderer getInstance()
    {
        if(instance == null)
            instance = new AutoAlignRenderer();
        return instance;
    }

    @SuppressWarnings("IntegerDivisionInFloatingPointContext")
    public void draw(GraphicsContext gc)
    {
        //NOTE: Top left is (0, 0)
        //TODO reset all values to 0 so we can update them later

        targetPoint = NetworkTableHandler.getInstance().getTargetCoords();
        centerPoint = NetworkTableHandler.getInstance().getCenterCoords();
        vector = NetworkTableHandler.getInstance().getAutoAlignVector();
        targetFound = NetworkTableHandler.getInstance().isTargetFound();

        gc.setLineWidth(5);
        //draw Target
        gc.setStroke(Color.LIME);

        if (targetFound && targetPoint.isValid())
            gc.strokeRect(targetPoint.getX() - SQUARE_SIDE / 2,
                    targetPoint.getY() - SQUARE_SIDE / 2,
                    SQUARE_SIDE,
                    SQUARE_SIDE);

        gc.setStroke(Color.AQUA);
        if (centerPoint.isValid()) {
            gc.strokeRect(centerPoint.getX() - SQUARE_SIDE / 2,
                    centerPoint.getY() - SQUARE_SIDE / 2,
                    SQUARE_SIDE,
                    SQUARE_SIDE);
            //pseudo-crosshair
            gc.setStroke(Color.WHITE);
            gc.setLineWidth(2);
            gc.strokeLine(centerPoint.getX(), centerPoint.getY() - SQUARE_SIDE/2, centerPoint.getX(), centerPoint.getY() + SQUARE_SIDE/2);
            gc.strokeLine(centerPoint.getX() - SQUARE_SIDE/2, centerPoint.getY(), centerPoint.getX() + SQUARE_SIDE/2, centerPoint.getY());
            if (targetFound && vector.isValid())
                gc.strokeLine(centerPoint.getX(), centerPoint.getY(), centerPoint.getX() + vector.getX(), centerPoint.getY() - vector.getY());
        }
    }
}
