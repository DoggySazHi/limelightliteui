package com.williamle.limelightliteui;

import com.williamle.limelightliteui.MagicValues.*;
import edu.wpi.first.networktables.*;
import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;

import java.time.Duration;
import java.time.Instant;

import static com.williamle.limelightliteui.MagicValues.*;

public class NetworkTableHandler
{
    private static NetworkTableHandler instance;

    private NetworkTableInstance ntinstance;

    private NetworkTableEntry colorR;
    private NetworkTableEntry colorG;
    private NetworkTableEntry colorB;
    private NetworkTableEntry mode;

    private NetworkTableEntry targetFound;
    private NetworkTableEntry autoAlignEnabled;
    private NetworkTableEntry autoAlignCoords;
    private NetworkTableEntry centerCoords;
    private NetworkTableEntry targetCoords;

    private NetworkTableEntry ultrasonicDist;

    //Remember to empty after reading
    private NetworkTableEntry logData;

    private NetworkTableHandler()
    {
        Log.w("Starting NetworkTables... (Check Java console for errors, as they cannot be redirected to this one!)");
        ntinstance = NetworkTableInstance.getDefault();
        ntinstance.startClientTeam(3952);
        //Only useful for Windows Driver Station
        ntinstance.startDSClient();
        init();
        startTime = Instant.now();

        new AnimationTimer() {
            @Override
            public void handle(long t)
            {
                loop();
            }
        }.start();
    }

    private void init()
    {
        NetworkTable limeLightLite = ntinstance.getTable("LimeLightLite");
        NetworkTable cameraLight = ntinstance.getTable("cameralight");

        colorR = cameraLight.getEntry("Red");
        colorG = cameraLight.getEntry("Green");
        colorB = cameraLight.getEntry("Blue");

        mode = limeLightLite.getEntry("Mode");
        targetFound = limeLightLite.getEntry("Target Found");
        autoAlignEnabled = limeLightLite.getEntry("AutoAlign Enabled");
        //the AutoAlign X/Y fields are for directional purposes
        autoAlignCoords = limeLightLite.getEntry("AutoAlign Coordinates");
        logData = limeLightLite.getEntry("Log");
        centerCoords = limeLightLite.getEntry("Center Coordinates");
        targetCoords = limeLightLite.getEntry("Target Coordinates");
        ultrasonicDist = cameraLight.getEntry("Ultrasonic Distance");

        colorR.setDouble(0.0);
        colorG.setDouble(0.0);
        colorB.setDouble(0.0);
        mode.setDefaultString("Idle");
        targetFound.setDefaultBoolean(false);
        autoAlignEnabled.setBoolean(false);
        autoAlignCoords.setDefaultDoubleArray(DEFAULT_VALUE);
        logData.setString("");
        centerCoords.setDefaultDoubleArray(DEFAULT_CENTER_COORDINATES);
        targetCoords.setDefaultDoubleArray(DEFAULT_VALUE);
        ultrasonicDist.setDefaultDouble(0.0);
        ultrasonicDist.addListener(o -> UltrasonicSensor.getInstance().updateSensor(o.value.getDouble()), EntryListenerFlags.kUpdate);
    }

    public static NetworkTableHandler getInstance()
    {
        if(instance == null)
            instance = new NetworkTableHandler();
        return instance;
    }

    private Instant startTime;
    private boolean fallBack = false;
    private boolean connected = false;

    private void loop()
    {
        //TODO Do we really need this (merge both regular and debug IPs)
        if (!fallBack && !ntinstance.isConnected() && Duration.between(startTime, Instant.now()).toMillis() > 10000) {
            Log.w("ERROR", "Failed to connect to the robot's NetworkTables server! Falling back to debug IPs.");
            fallBack = true;
            ntinstance.startClient(DEBUG_NT_SERVERS);
        }

        if(!connected && ntinstance.isConnected())
        {
            connected = true;
            Log.w("Connected to a NetworkTables server at " + ntinstance.getConnections()[0].remote_ip + "!");
            MainController.getInstance().setNTIP(ntinstance.getConnections()[0].remote_ip);
            init();
        }

        if(connected && !ntinstance.isConnected())
        {
            startTime = Instant.now();
            connected = false;
            fallBack = false;
            Log.w("Lost connection to the NetworkTables server! Resetting.");
        }

        boolean aae = autoAlignEnabled.getBoolean(false);
        boolean tf = targetFound.getBoolean(false);
        MainController instance = MainController.getInstance();

        //what did i do wrong to do this
        if(aae)
            if(tf)
                instance.setAutoAlignStatus(Status.ACTIVE);
            else
                instance.setAutoAlignStatus(Status.LOST);
        else
            if(tf)
                instance.setAutoAlignStatus(Status.ARMED);
            else
                instance.setAutoAlignStatus(Status.DISARMED);

        instance.setColorPicker(
                colorR.getDouble(0.0),
                colorG.getDouble(0.0),
                colorB.getDouble(0.0));

        String log = logData.getString("");
        if(log.contains("\n"))
        {
            String[] data = log.split("\n");
            for(String line : data)
                if(line.length() >= 2)
                    Log.w(line);
        }

        instance.setCoordinates("Center: " + getCenterCoords() + " Target: " + getTargetCoords() + " Vector: " + getAutoAlignVector());
        instance.setData("Distance: " + UltrasonicSensor.getInstance().getValue());
    }

    public void toggleAutoAlign()
    {
        if(autoAlignEnabled.getBoolean(false))
        {
            Log.w("AutoAlign disabled!");
            autoAlignEnabled.setBoolean(false);
            return;
        }
        Log.w("AutoAlign enabled!");
        autoAlignEnabled.setBoolean(true);
    }

    public Point getAutoAlignVector()
    {
        return new Point(autoAlignCoords.getDoubleArray(DEFAULT_VALUE));
    }

    public Point getCenterCoords()
    {
        return new Point(centerCoords.getDoubleArray(DEFAULT_VALUE));
    }

    public Point getTargetCoords()
    {
        return new Point(targetCoords.getDoubleArray(DEFAULT_VALUE));
    }

    public LightMode getMode()
    {
        LightMode lm = null;
        try {
            lm = LightMode.valueOf(mode.getString("Idle"));
        }
        catch(IllegalArgumentException ex)
        {
            Log.w("ERROR", "Got an invalid mode from the NetworkTables server: "
                    + mode.getString("FAILED_TO_GET_STRING"));
        }
        return lm;
    }

    public void setMode(LightMode lm)
    {
        mode.setString(lm.toString());
    }

    public void setColor(Color c)
    {
        colorR.setDouble((int)(c.getRed() * 255));
        colorG.setDouble((int)(c.getGreen() * 255));
        colorB.setDouble((int)(c.getBlue() * 255));
    }

    public boolean isConnected()
    {
        return connected;
    }

    public boolean isTargetFound() {
        return targetFound.getBoolean(false);
    }

    public void setNTServer(String ip) {
        ConnectionInfo[] connections = ntinstance.getConnections();
        if (connections.length > 0 && connections[0].remote_ip.equals(ip))
            return;
        ntinstance.startClient(ip);
    }
}
