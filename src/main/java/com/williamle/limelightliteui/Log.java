package com.williamle.limelightliteui;

import javafx.application.Platform;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Log
{
    private Log()
    {
        //prevent people from init-ing
    }

    public static void w(String title, String text)
    {
        StringBuffer stringBuffer = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
        Date dt = new Date();
        if(title != null && !title.equals(""))
            stringBuffer.append("[").append(title).append("] ");
        else
            stringBuffer.append("[INFO]").append(" ");
        stringBuffer.append("[")
            .append(sdf.format(dt))
            .append("]: ")
            .append(text)
            .append("\n");
        Platform.runLater(() -> MainController.getInstance().appendLogText(stringBuffer.toString()));
    }

    public static void w(String text)
    {
        text = text.replace("\n", "");
        w(null, text);
    }
}
