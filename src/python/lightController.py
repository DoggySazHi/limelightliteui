import time

lightStop = False
r = 0
g = 0
b = 0

# Stolen from NeoPixel libraries.


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 85:
        return int(pos * 3), int(255 - (pos * 3)), 0
    elif pos < 170:
        pos -= 85
        return int(255 - (pos * 3)), 0, int(pos * 3)
    else:
        pos -= 170
        return 0, int(pos * 3), int(255 - pos * 3)


def idleAnim(pixels):
    global lightStop
    lightStop = False
    while True:
        for j in range(255):
            for i in range(len(pixels)):
                idx = int((i * 256 / len(pixels)) + j * 10)
                pixels[i] = wheel(idx & 255)
            pixels.show()
            if lightStop:
                lightsOff(pixels)
                return
            time.sleep(0.001)


def autoAlignAnim(pixels):
    global lightStop
    lightStop = False
    pixels.fill((0, 255, 0))
    pixels.show()
    if lightStop:
        lightsOff(pixels)
        return


def brownOutAnim(pixels):
    global lightStop
    lightStop = False
    lastTrigger = time.time()
    lightOn = True
    while True:
        if time.time() - lastTrigger > 0.5:
            lastTrigger = time.time()
            lightOn = not lightOn
        if lightOn:
            pixels.fill((255, 0, 0))
            pixels.show()
        else:
            pixels.fill((0, 0, 0))
            pixels.show()
        if lightStop:
            lightsOff(pixels)
            return


def warningAnim(pixels):
    global lightStop
    lightStop = False
    lastTrigger = time.time()
    lightOn = True
    while True:
        if time.time() - lastTrigger > 1:
            lastTrigger = time.time()
            lightOn = not lightOn
        if lightOn:
            pixels.fill((255, 255, 0))
            pixels.show()
        else:
            pixels.fill((0, 0, 0))
            pixels.show()
        if lightStop:
            lightsOff(pixels)
            return


def manualAnim(pixels):
    global lightStop, r, g, b
    lightStop = False
    while True:
        if lightStop:
            lightsOff(pixels)
            return
        r = max(0, min(r, 255))
        g = max(0, min(g, 255))
        b = max(0, min(b, 255))
        # sd.putNumber("Red", r)
        # sd.putNumber("Green", g)
        # sd.putNumber("Blue", b)
        pixels.fill(((int(float(r)), int(float(g)), int(float(b)))))


def setRGB(rr, gg, bb):
    global r, g, b
    r = rr
    g = gg
    b = bb


def lightsOff(pixels):
    global lightStop
    lightStop = False
    pixels.fill((0, 0, 0))
    pixels.show()


def kill():
    global lightStop
    lightStop = True
