package com.williamle.limelightliteui;

public class Point
{
    public double x, y;
    private boolean isValid = true;

    public Point()
    {
        x = 0;
        y = 0;
        isValid = false;
    }

    public Point(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public Point(Point p)
    {
        this.x = p.x;
        this.y = p.y;
    }

    public Point(double[] in)
    {
        if (in.length != 2) {
            Log.w("WARNING", "Attempted to create a point with a bad array!");
            isValid = false;
            return;
        }
        this.x = in[0];
        this.y = in[1];
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    @Deprecated
    public void setPoint(Point p)
    {
        //are you dumb
        this.x = p.x;
        this.y = p.y;
    }

    @Override
    public String toString()
    {
        return "(" + x + ", " + y + ")";
    }

    public double[] toArray()
    {
        return new double[]{x, y};
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof Point) {
            Point test = (Point) o;
            return test.x == x && test.y == y;
        }
        return false;
    }

    public boolean isValid()
    {
        return isValid;
    }
}